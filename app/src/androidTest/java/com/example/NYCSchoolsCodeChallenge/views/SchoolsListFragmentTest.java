package com.example.NYCSchoolsCodeChallenge.views;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.RootMatchers;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;

import com.example.NYCSchoolsCodeChallenge.R;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;



public class SchoolsListFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);

    private MainActivity mActivity = null;
    @Before
    public void setUp() throws Exception {

        mActivity = activityRule.getActivity();
    }

//    @Test
//    public void testlauch(){
//        //test if the fragment is launched or not
//        FrameLayout rlContainer = mActivity.findViewById(R.id.fragment_placeholder);
//        assertNotNull(rlContainer);
//        SchoolsListFragment fragment = new SchoolsListFragment();
//        getInstrumentation().waitForIdleSync();
//        mActivity.getSupportFragmentManager().beginTransaction().add(rlContainer.getId(),fragment).commitAllowingStateLoss();
//        View view = Objects.requireNonNull(fragment.getView()).findViewById(R.id.fragment_placeholder);
//        assertNotNull(view);
//
//    }

    @Test
    public void testSampleRecyclerVisible() {
        Espresso.onView(ViewMatchers.withId(R.id.recyclerView))
                .inRoot(RootMatchers.withDecorView(
                        Matchers.is(activityRule.getActivity().getWindow().getDecorView())))
                .check(matches(isDisplayed()));
    }

    @Test
    public void testCaseForRecyclerScroll() {

        // Get total item of RecyclerView
        RecyclerView recyclerView = activityRule.getActivity().findViewById(R.id.recyclerView);
        int itemCount = recyclerView.getAdapter().getItemCount();

        // Scroll to end of page with position
        Espresso.onView(ViewMatchers.withId(R.id.recyclerView))
                .inRoot(RootMatchers.withDecorView(
                        Matchers.is(activityRule.getActivity().getWindow().getDecorView())))
                .perform(RecyclerViewActions.scrollToPosition(itemCount - 2));
    }


    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}