package com.example.NYCSchoolsCodeChallenge.views;

import android.view.View;
import androidx.test.rule.ActivityTestRule;
import com.example.NYCSchoolsCodeChallenge.R;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule =
            new ActivityTestRule<>(MainActivity.class);

    private MainActivity mainActivity = null;

    @Before
    public void setUp() throws Exception {
        mainActivity = activityRule.getActivity();
    }

    @Test
    public void mainActivityTestLaunch(){
        // testing main activity launch
        View view = mainActivity.findViewById(R.id.recyclerView);
        assertNotNull(view);
    }


    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}