package com.example.NYCSchoolsCodeChallenge.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import com.example.NYCSchoolsCodeChallenge.model.SchoolPerformance;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class SchoolPerformanceViewModelTest {
    private MutableLiveData<ArrayList<SchoolPerformance>> schoolDetailList;
    private String dbn;

    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

    @Mock
    SchoolPerformanceViewModel schoolPerformanceViewModel;
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testgetSchoolRepository(){
        when(schoolPerformanceViewModel.getSchoolRepository(dbn)).thenReturn(schoolDetailList);
        System.out.println(schoolPerformanceViewModel.getSchoolRepository(dbn));

    }

    @After
    public void tearDown() throws Exception {
    }
}