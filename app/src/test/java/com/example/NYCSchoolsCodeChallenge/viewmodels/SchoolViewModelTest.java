package com.example.NYCSchoolsCodeChallenge.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;

import com.example.NYCSchoolsCodeChallenge.model.School;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SchoolViewModelTest {
    private MutableLiveData<ArrayList<School>> schoolList = new MutableLiveData<>();

    ArrayList<String> testArraylist = new ArrayList<>(Arrays.asList("abc","efg"));


    @Rule
    public InstantTaskExecutorRule instantExecutorRule = new InstantTaskExecutorRule();

//    @Spy
//    SchoolViewModel schoolViewModel;

    @Mock
    SchoolViewModel schoolViewModel;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testgetSchoolRepository(){
        //doReturn(schoolViewModel.getSchoolRepository()).when(schoolViewModel).getSchoolRepository();
        when(schoolViewModel.getSchoolRepository()).thenReturn(schoolList);
        System.out.println(schoolViewModel.getSchoolRepository());
    }

    @After
    public void tearDown() throws Exception {
    }
}