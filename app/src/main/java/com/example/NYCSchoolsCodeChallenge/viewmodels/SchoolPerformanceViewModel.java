package com.example.NYCSchoolsCodeChallenge.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.NYCSchoolsCodeChallenge.model.SchoolPerformance;
import com.example.NYCSchoolsCodeChallenge.repository.SchoolRepository;
import java.util.ArrayList;


public class SchoolPerformanceViewModel extends ViewModel {

    private MutableLiveData<ArrayList<SchoolPerformance>> schoolDetailList;
    private SchoolRepository schoolRepository;
    public LiveData<ArrayList<SchoolPerformance>> getSchoolRepository(String dbn) {

        // If ArrayList is null loading the data asynchronously from the server in getSchoolPerformances method
        if (schoolDetailList == null) {

            schoolRepository = SchoolRepository.getInstance();
            schoolDetailList = schoolRepository.getSchoolPerformances(dbn);
        }
        return schoolDetailList;
    }
}
