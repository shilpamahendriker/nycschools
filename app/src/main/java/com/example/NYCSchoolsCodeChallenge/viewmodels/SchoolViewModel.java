package com.example.NYCSchoolsCodeChallenge.viewmodels;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.NYCSchoolsCodeChallenge.model.School;
import com.example.NYCSchoolsCodeChallenge.repository.SchoolRepository;
import java.util.ArrayList;


public class SchoolViewModel extends ViewModel {
    //this is the data that will fetch asynchronously
    private MutableLiveData<ArrayList<School>> schoolList;
    private SchoolRepository schoolRepository;

    public void init(){
        if (schoolList != null){
            return;
        }
        schoolRepository = SchoolRepository.getInstance();
        schoolList = schoolRepository.getSchools();
    }

    public LiveData<ArrayList<School>> getSchoolRepository() {
        return schoolList;
    }


}
