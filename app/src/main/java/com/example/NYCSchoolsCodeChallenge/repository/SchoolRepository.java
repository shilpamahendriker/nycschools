package com.example.NYCSchoolsCodeChallenge.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.NYCSchoolsCodeChallenge.model.School;
import com.example.NYCSchoolsCodeChallenge.model.SchoolPerformance;
import com.example.NYCSchoolsCodeChallenge.network.APIService;
import com.example.NYCSchoolsCodeChallenge.network.RetrofitService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//This class provides network request for hitting API and using LiveData for observing API response.
public class SchoolRepository {
    private static SchoolRepository schoolRepository;
    private APIService apiService;

    public SchoolRepository() {
    }

    public static SchoolRepository getInstance() {
        if (schoolRepository == null) {
            schoolRepository = new SchoolRepository();
        }
        return schoolRepository;
    }

    // For listing the schools
    public MutableLiveData<ArrayList<School>> getSchools() {
        final MutableLiveData<ArrayList<School>> schoolList = new MutableLiveData<>();
        apiService = RetrofitService.createService(APIService.class);
        Call<ArrayList<School>> call = apiService.getSchools();
        call.enqueue(new Callback<ArrayList<School>>() {
            @Override
            public void onResponse(Call<ArrayList<School>> call, Response<ArrayList<School>> response) {
                //finally set the list to MutableLiveData
                schoolList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<School>> call, Throwable t) {
                schoolList.setValue(null);
            }
        });
        return schoolList;
    }

    //For listing school details of the selected school
    public MutableLiveData<ArrayList<SchoolPerformance>> getSchoolPerformances(String dbn) {
        final MutableLiveData<ArrayList<SchoolPerformance>> schoolDetailList = new MutableLiveData<>();
        apiService = RetrofitService.createService(APIService.class);
        Call<ArrayList<SchoolPerformance>> call = apiService.getSchoolDetails(dbn);
        call.enqueue(new Callback<ArrayList<SchoolPerformance>>() {
            @Override
            public void onResponse(Call<ArrayList<SchoolPerformance>> call, Response<ArrayList<SchoolPerformance>> response) {
                //finally set the list to MutableLiveData
                schoolDetailList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolPerformance>> call, Throwable t) {
                schoolDetailList.setValue(null);
            }
        });
        return schoolDetailList;
    }
}