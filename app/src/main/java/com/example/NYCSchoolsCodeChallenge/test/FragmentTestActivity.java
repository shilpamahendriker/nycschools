package com.example.NYCSchoolsCodeChallenge.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.NYCSchoolsCodeChallenge.R;

public class FragmentTestActivity extends AppCompatActivity {

    // Using this  activity for fragment isolation testing
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_test);
    }
}
